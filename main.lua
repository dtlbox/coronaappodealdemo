-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local appodeal = require( "plugin.appodeal" )
local widget = require( "widget" )
local json = require( "json" )

--------------------------------------------------------------------------
-- set up local variables
--------------------------------------------------------------------------

local availableAdTypes = {"banner", "interstitial", "video", "rewardedVideo"}
local platform = system.getInfo("platform")
local eventText = nil
local appKey = nil

-- insert your appKeys down below
if platform == "android" then
	appKey = ""
elseif platform == "ios" then
	appKey = ""
end

print("appKey ~~> " .. appKey)

--------------------------------------------------------------------------
-- set up framework functions
--------------------------------------------------------------------------

  local processEventTable = function(event)
      local logString = json.prettify(event):gsub("\\","")
      logString = "\nPHASE: "..event.phase.." - - - - - - - - - - - -\n" .. logString
      print(logString)
      return logString
  end

  local function showBannerTop()
    appodeal.show("banner", { yAlign="top" })
  end

  local function showBannerBottom()
    appodeal.show("banner", { yAlign="bottom" })
  end

  local function showInterstitial()
    appodeal.show("interstitial", { })
  end

  local function showVideo()
    appodeal.show("video", { })
  end

  local function showRewardedVideo()
    appodeal.show("rewardedVideo", { })
  end

  local function appodealListener( event )
  	eventText.text = processEventTable(event)

    -- Exit function if user hasn't set up testing parameters
  	if ( setupComplete == false ) then return end

  	-- Successful initialization of the Appodeal plugin
  	if ( event.phase == "init" ) then
  		eventText.text = "Appodeal event: initialization successful"

  	-- An ad loaded successfully
  	elseif ( event.phase == "loaded" ) then
  		eventText.text = "Appodeal event: " .. tostring(event.type) .. " ad loaded successfully"

  	-- The ad was displayed/played
  	elseif ( event.phase == "displayed" or event.phase == "playbackBegan" ) then
  		eventText.text = "Appodeal event: " .. tostring(event.type) .. " ad displayed"

  	-- The ad was closed/hidden/completed
  	elseif ( event.phase == "hidden" or event.phase == "closed" or event.phase == "playbackEnded" ) then
  		eventText.text = "Appodeal event: " .. tostring(event.type) .. " ad closed/hidden/completed"

  	-- The user clicked/tapped an ad
  	elseif ( event.phase == "clicked" ) then
  		eventText.text = "Appodeal event: " .. tostring(event.type) .. " ad clicked/tapped"

  	-- The ad failed to load
  	elseif ( event.phase == "failed" ) then
  		eventText.text = "Appodeal event: " .. tostring(event.type) .. " ad failed to load"
  	end
  end

  print("Using: ", appKey)

  appodeal.init(appodealListener, {
  	appKey = appKey,
  	supportedAdTypes = availableAdTypes,
  	bannerAnimation = true
  })

--------------------------------------------------------------------------
-- set up UI
--------------------------------------------------------------------------
display.setStatusBar( display.HiddenStatusBar )

-- Create the background

local background = display.newImageRect("back-whiteorange.png", display.actualContentWidth, display.actualContentHeight)
background.x = display.contentCenterX
background.y = display.contentCenterY

-- Create label

eventText = display.newText {
  text = "Initializing...",
  font = native.systemFont,
  fontSize = 12,
  align = "left",
  width = 310,
  height = 200,
}
eventText.anchorX = 0
eventText.anchorY = 0
eventText.x = display.screenOriginX + 5
eventText.y = 0
eventText:setFillColor(0)

-- Create buttons

local button0 = widget.newButton(
{
  id = "banner",
  label = "Banner Top",
  labelColor = { default={ 250 / 255, 100 / 255, 60 / 255 }, over={ 0, 0, 0, 0.5 } },
  width = 250,
  onRelease = function(event)
    showBannerTop()
  end,
})
button0.x = display.contentCenterX
button0.y = 150

local button1 = widget.newButton(
{
  id = "banner",
  label = "Banner Bottom",
  labelColor = { default={ 250 / 255, 100 / 255, 60 / 255 }, over={ 0, 0, 0, 0.5 } },
  width = 250,
  onRelease = function(event)
    showBannerBottom()
  end,
})
button1.x = display.contentCenterX
button1.y = button0.y + button0.height + button1.height * .15

local button2 = widget.newButton(
{
  id = "interstitial",
  label = "Interstitial",
  labelColor = { default={ 250 / 255, 100 / 255, 60 / 255 }, over={ 0, 0, 0, 0.5 } },
  onRelease = function(event)
    showInterstitial()
  end,
})
button2.x = display.contentCenterX
button2.y = button1.y + button1.height + button2.height * .15

local button3 = widget.newButton(
{
  id = "video",
  label = "Video",
  labelColor = { default={ 250 / 255, 100 / 255, 60 / 255 }, over={ 0, 0, 0, 0.5 } },
  onRelease = function(event)
    showVideo()
  end,
})
button3.x = display.contentCenterX
button3.y = button2.y + button2.height + button3.height * .15

local button4 = widget.newButton(
{
  id = "rewardedVideo",
  label = "Rewarded video",
  labelColor = { default={ 250 / 255, 100 / 255, 60 / 255 }, over={ 0, 0, 0, 0.5 } },
  onRelease = function(event)
    showRewardedVideo()
  end,
})
button4.x = display.contentCenterX
button4.y = button3.y + button3.height + button3.height * .15
